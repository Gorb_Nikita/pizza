

var jQuery = require("jQuery")


window.$ = jQuery

if (jQuery) {
    require("owl.carousel/dist//owl.carousel.min")
    require("./js/main.js")
    require("./scss/required.scss")
}
