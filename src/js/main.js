import Slideout from "slideout"
$(document).ready(function() {
    var slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'padding': 100,
        'tolerance': 10,
        'transition': .3,
        'side': "right"
      });
    
      // Toggle button
      document.querySelector('.toggle-button').addEventListener('click', function() {
        slideout.toggle();
      });
      document.querySelector('#close').addEventListener('click', function() {
        slideout.close();
      });
      
      $(".owl-carousel").owlCarousel(
        {
          items:1,
          loop:true,
          nav:true,
          navText: ["&lang;", "&rang;"],
          dotsEach:true,
        }
      );

});

let 
  popup = document.getElementById('mypopup'),
  popupTogle = document.getElementById('myBtn'),
  popupClose = document.querySelector('.close');
  // popupStop = document.querySelector('.stop');
  

  popupTogle.onclick = function() {
  popup.style.display="block";
  };

  popupClose.onclick=function(){
    popup.style.display="none";
  };
  // popupStop.onclick=function(){
  //   popup.style.display="none";
  // };

window.onclick = function(e){
  if(e.target==popup){
    popup.style.display="none";
  }
}

 

