const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const GoogleFontsPlugin = require("google-fonts-webpack-plugin")
const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: './src/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
  },
  module: {
    rules: [
      {
        test: /\.pug$/,
        loader: 'pug-loader',
        options: {
        pretty: true
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
      name: '[name].[ext]',
     }
            },
            
          ]
        
      },



    ]
  },
  plugins: [
    new ExtractTextPlugin('style.css'),
    new HtmlWebpackPlugin({
        template: "./src/index.pug",
        filename: "index.html",
        minify: false,
        cache: false,
    }),
    new CopyPlugin({
      patterns: [
        { from: "src/assets/", to: "assets/" },
      ],
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new HtmlWebpackPlugin({
      template: "./src/basket.pug",
      filename: "basket.html",
      minify: false,
      cache: false,
  }),
    new HtmlWebpackPlugin({
      template: "./src/delivery.pug",
      filename: "delivery.html",
      minify: false,
      cache: false,
  }),
  new HtmlWebpackPlugin({
    template: "./src/deliveryOrder.pug",
    filename: "deliveryOrder.html",
    minify: false,
    cache: false,
}),
new HtmlWebpackPlugin({
  template: "./src/lovePizza.pug",
  filename: "lovePizza.html",
  minify: false,
  cache: false,
}),
new HtmlWebpackPlugin({
  template: "./src/feedback.pug",
  filename: "feedback.html",
  minify: false,
  cache: false,
}),

    

   ]
};